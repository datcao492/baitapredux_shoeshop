import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART } from "./redux/constants/constant";

class ProductItems extends Component {
  render() {
    let { image } = this.props.data;
    let { name } = this.props.data;
    let { price } = this.props.data;

    return (
      <div className="card col-4">
        <img className="card-img-top" src={image} alt="" />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
        </div>
        <p className="card-text">$ {price}</p>
        <button
          onClick={() => {
            this.props.handleAddToCart(this.props.data);
          }}
          className="btn btn-warning mb-2"
        >
          Add to cart
        </button>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (product) => {
      dispatch({ type: ADD_TO_CART, payload: product });
    },
  };
};

export default connect(null, mapDispatchToProps)(ProductItems);
