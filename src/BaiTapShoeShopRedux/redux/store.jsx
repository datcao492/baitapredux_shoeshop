import { createStore } from "redux";
import { rootReduce } from "./reducers/rootReduce";

export const storeShoeShop = createStore(
  rootReduce,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
