import { shoeShopReducer } from "./shoeShopReducer";
import { combineReducers } from "redux";

export const rootReduce = combineReducers({
  shoeShopReducer,
});
