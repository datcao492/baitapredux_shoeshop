import { dataShoeShop } from "../../dataShoeShop";
import {
  ADD_TO_CART,
  DELETE_ITEM,
  DOWN_QUANTITY,
  MODAL_CLOSE,
  MODAL_OK,
  REDUCE_QUANTITY,
} from "../constants/constant";
let initialState = {
  productList: dataShoeShop,
  gioHang: [],
  isOpenModal: false,
  deleteProduct: -1,
};

export const shoeShopReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let cloneGioHang = [...state.gioHang];
      let index = cloneGioHang.findIndex((item) => {
        return item.id == payload.id;
      });

      if (index == -1) {
        let newProduct = { ...payload, quantity: 1 };
        cloneGioHang.push(newProduct);
      } else {
        cloneGioHang[index].quantity++;
      }

      state.gioHang = cloneGioHang;
      let dataJson = JSON.stringify(state);
      state = JSON.parse(dataJson);
      return { ...state };
    }

    case DELETE_ITEM: {
      let cloneGioHang = [...state.gioHang];
      let index = state.gioHang.findIndex((item) => {
        return item.id == payload;
      });
      if (index !== -1) {
        cloneGioHang.splice(index, 1);
      }
      state.gioHang = cloneGioHang;
      let dataJson = JSON.stringify(state);
      state = JSON.parse(dataJson);
      return { ...state };
    }

    case REDUCE_QUANTITY: {
      let cloneGioHang = [...state.gioHang];
      let index = cloneGioHang.findIndex((item) => {
        return item.id == payload.id;
      });

      cloneGioHang[index].quantity++;

      state.gioHang = cloneGioHang;
      let dataJson = JSON.stringify(state);
      state = JSON.parse(dataJson);
      return { ...state };
    }

    case DOWN_QUANTITY: {
      let cloneGioHang = [...state.gioHang];
      let index = cloneGioHang.findIndex((item) => {
        return item.id == payload.id;
      });

      if ((cloneGioHang[index].quantity = 1)) {
        state.isOpenModal = true;
        state.deleteProduct = index;
      } else {
        cloneGioHang[index].quantity--;
      }
      state.gioHang = cloneGioHang;
      let dataJson = JSON.stringify(state);
      state = JSON.parse(dataJson);
      return { ...state };
    }

    case MODAL_OK: {
      state.isOpenModal = false;

      let cloneGioHang = [...state.gioHang];
      cloneGioHang.splice(state.deleteProduct, 1);

      state.gioHang = cloneGioHang;
      let dataJson = JSON.stringify(state);
      state = JSON.parse(dataJson);
      return { ...state };
    }

    case MODAL_CLOSE: {
      state.isOpenModal = false;

      let dataJson = JSON.stringify(state);
      state = JSON.parse(dataJson);
      return { ...state };
    }

    default:
      return state;
  }
};
