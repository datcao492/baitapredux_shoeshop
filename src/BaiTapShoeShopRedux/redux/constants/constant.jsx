export const ADD_TO_CART = "Add to cart";
export const DELETE_ITEM = "Delete item";
export const SUB_TOTAL = "Sub total";
export const MODAL_OK = "OK delete";
export const MODAL_CLOSE = "Close Modal";
export const UP_DOWN_QUANTITY = "UP DOWN QUANTITY";
export const REDUCE_QUANTITY = "UP QUANTITY";
export const DOWN_QUANTITY = "DOWN QUANTITY";
