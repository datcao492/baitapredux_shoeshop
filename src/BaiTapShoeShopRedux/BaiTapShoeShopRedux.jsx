import React, { Component } from "react";
import { connect } from "react-redux";
import Cart from "./Cart";
import ProduceList from "./ProduceList";

class BaiTapShoeShopRedux extends Component {
  render() {
    let totalQuantity = this.props.gioHang.reduce(
      (accumulator, currentValue) => {
        return accumulator + currentValue.quantity;
      },
      0
    );
    return (
      <div className="d-flex py-5 px-1">
        <div className="col-7">
          <ProduceList />
        </div>
        <div className="col-5">
          <h5 className="text-right mr-2">Gio hang: {totalQuantity}</h5>
          {this.props.gioHang.length != 0 && <Cart />}
        </div>
      </div>
    );
  }
}

let mapstateToProps = (state) => {
  return {
    gioHang: state.shoeShopReducer.gioHang,
    isOpenModal: state.shoeShopReducer.isOpenModal,
  };
};
export default connect(mapstateToProps)(BaiTapShoeShopRedux);
