import React, { Component } from "react";
import { connect } from "react-redux";
import ProduceItem from "./ProduceItem";
class ProductList extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          {this.props.productList?.map((item, index) => {
            return <ProduceItem data={item} key={index} />;
          })}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    productList: state.shoeShopReducer.productList,
  };
};

export default connect(mapStateToProps)(ProductList);
