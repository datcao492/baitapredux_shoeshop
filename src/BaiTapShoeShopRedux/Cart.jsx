import React, { Component } from "react";
import { connect } from "react-redux";
import { Modal } from "antd";
import {
  DELETE_ITEM,
  DOWN_QUANTITY,
  MODAL_CLOSE,
  MODAL_OK,
  REDUCE_QUANTITY,
} from "./redux/constants/constant";

class Cart extends Component {
  render() {
    let Total = this.props.gioHang.reduce((accumulator, currentvalue) => {
      return accumulator + currentvalue.quantity * currentvalue.price;
    }, 0);

    return (
      <div className="">
        <Modal
          title="Basic Modal"
          visible={this.props.isOpenModal}
          onOk={() => {
            this.props.handleModalOK();
          }}
          onCancel={() => {
            this.props.handleModalClose();
          }}
        >
          <p className="text-danger">Are you sure to delete this?</p>
        </Modal>

        <table className="table text-center">
          <thead>
            <tr>
              <td>Item</td>
              <td></td>
              <td>Price</td>
              <td>Quantity</td>
              <td>TOTAL</td>
              <td>
                <i className="fab fa-whmcs"></i>
              </td>
            </tr>
          </thead>
          <tbody>
            {this.props.gioHang.map((item) => {
              return (
                <tr>
                  <td>{item.name}</td>
                  <td>
                    <img
                      className="p-0 m-0"
                      style={{ width: "70px", height: "70px" }}
                      src={item.image}
                      alt=""
                    />
                  </td>
                  <td>$ {item.price}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleDownQuantity(item);
                      }}
                      className="btn btn-danger mx-2"
                    >
                      -
                    </button>
                    {item?.quantity}
                    <button
                      onClick={() => {
                        this.props.handleUpQuantity(item);
                      }}
                      className="btn btn-success mx-2"
                    >
                      +
                    </button>
                  </td>
                  <td>$ {`${item.price}` * `${item.quantity}`}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleDeleteProduct(item.id);
                      }}
                      className="btn btn-danger"
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
          <tfoot>
            <tr>
              <td> </td>
              <td></td>
              <td></td>
              <td className="text-warning font-weight-bold">SUBTOTAL:</td>
              <td className="text-danger font-weight-bold">$ {Total}</td>
            </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeShopReducer.gioHang,
    isOpenModal: state.shoeShopReducer.isOpenModal,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleDeleteProduct: (idItem) => {
      dispatch({ type: DELETE_ITEM, payload: idItem });
    },

    handleUpQuantity: (idItem) => {
      dispatch({
        type: REDUCE_QUANTITY,
        payload: idItem,
      });
    },

    handleDownQuantity: (idItem) => {
      dispatch({
        type: DOWN_QUANTITY,
        payload: idItem,
      });
    },

    handleModalOK: () => {
      dispatch({
        type: MODAL_OK,
      });
    },

    handleModalClose: () => {
      dispatch({
        type: MODAL_CLOSE,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
